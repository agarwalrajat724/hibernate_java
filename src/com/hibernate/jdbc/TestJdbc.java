/**
 * 
 */
package com.hibernate.jdbc;

import java.sql.Connection;
import java.sql.DriverManager;

/**
 * @author rajat_agarwal
 *
 */
public class TestJdbc {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		String jdbcUrl = "jdbc:mysql://localhost:3306/hb_student_tracker?useSSL=false&serverTimezone=UTC";
		String userId = "hbstudent";
		String passw = "hbstudent";

		try {
			
			System.out.println("Connecting to Database : " + jdbcUrl );
			Connection myConnection = DriverManager.getConnection(jdbcUrl, userId, passw);
			
			System.out.println("Connection SuccessFull");
			
		} catch(Exception ex) {
			ex.printStackTrace();
		}
	}

}
