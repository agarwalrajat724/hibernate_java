/**
 * 
 */
package com.hibernate.demo;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

import com.hibernate.demo.entity.Student;

/**
 * @author raagar
 *
 */
public class UpdateStudentDemo {

	/**
	 * @param args
	 */
	public static void main(String[] args) {

		SessionFactory factory =  new Configuration().configure("hibernate.cfg.xml")
										.addAnnotatedClass(Student.class)
										.buildSessionFactory();
		
		Session session = factory.getCurrentSession();
		
		try {
			
			
//			Student student = new Student("Hitti", "Babu_babu", "hitti_babu@email.com");
//			
//			session.beginTransaction();
//			
//			session.save(student);
//			
//			session.getTransaction().commit();
			
			session = factory.getCurrentSession();
			
			session.beginTransaction();
			
			Student studentRetr = session.get(Student.class, 7);
			
			
			studentRetr.setFirstName("Update_First Retr");
			// System.out.println("Saving Student : " + student.toString());
			
			System.out.println("Saving Student : " + studentRetr.toString());
			
			
			session.getTransaction().commit();
			
			session = factory.getCurrentSession();
			session.beginTransaction();
			
			
			// Will update all the records in the Table
			session.createQuery("update Student set email='updateRetr@email.com'").executeUpdate();
			
			session.getTransaction().commit();
			
			System.out.println("Done");
			
		} finally {
			factory.close();
		}
	}

}
