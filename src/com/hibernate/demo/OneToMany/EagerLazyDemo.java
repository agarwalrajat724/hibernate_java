package com.hibernate.demo.OneToMany;

import javax.persistence.Query;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

import com.hibernate.demo.OneToOne.Instructor;
import com.hibernate.demo.OneToOne.InstructorDetail;

public class EagerLazyDemo {

	public static void main(String[] args) {

		SessionFactory factory = new Configuration().configure("hibernate.cfg.xml").addAnnotatedClass(Course.class)
				.addAnnotatedClass(Instructor.class).addAnnotatedClass(InstructorDetail.class).buildSessionFactory();

		Session session = factory.getCurrentSession();

		try {
			session.beginTransaction();
			
			int theId = 9;
			
			org.hibernate.query.Query<Instructor> query = session.createQuery("select i from Instructor i " 
																+ " JOIN FETCH i.courses "
																+ " where i.id =:theInstructorId", Instructor.class);
			
			query.setParameter("theInstructorId", theId);
			
			Instructor tempInstructor =  query.getSingleResult();
			
			System.out.println(" Instructor : " + tempInstructor);
			
			session.getTransaction().commit();
			session.close();
			
			System.out.println("Course : " + tempInstructor.getCourses().get(0).toString());
			
		} catch (Exception e) {
			// TODO: handle exception
		} finally {
			session.close();
			factory.close();
		}
	}

}
