package com.hibernate.demo.OneToOne;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

public class CreateDemo {

	public static void main(String[] args) {

		SessionFactory sessionFactory = new Configuration()
											.configure("hibernate.cfg.xml")
											.addAnnotatedClass(Instructor.class)
											.addAnnotatedClass(InstructorDetail.class)
											.buildSessionFactory();
		
		Session session = sessionFactory.getCurrentSession();
		
		try {
			
			Instructor instructor = new Instructor("Rajahghjt", "Agarhghjwal", "agarwalrahghjjat724@gmail.com");
			InstructorDetail instructorDetail = new InstructorDetail("www.jhskjd.test.com", "Tenlhsjdnis");
			
			instructor.setInstructorDetail(instructorDetail);
			
			session.beginTransaction();
			
			// This will also save the instruction detail object because of Cascade Type ALL
			session.save(instructor);
			
			
			System.out.println(instructor);
			System.out.println(instructor.getInstructorDetail());
			
			
			session.getTransaction().commit();
			
			
			session = sessionFactory.getCurrentSession();
			session.beginTransaction();
			
			InstructorDetail detail = session.get(InstructorDetail.class, instructorDetail.getId());
			
			System.out.println(detail);
			
			System.out.println("Done!!!");
			
		} catch(Exception e) {
			
		}
	}

}
