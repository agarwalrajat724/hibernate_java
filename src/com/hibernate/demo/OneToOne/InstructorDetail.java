package com.hibernate.demo.OneToOne;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToOne;
import javax.persistence.Table;

@Entity
@Table(name="instructor_detail")
public class InstructorDetail {
	
	@Id
	@Column(name="id")
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private int id;
	
	@Column(name="youtube_channel")
	private String youtubeUrl;
	
	@Column(name="hobby")
	private String hobby;
	
	
//	More on mappedBy
//	 - mappedBy tells Hibernate
//	 Look at the instructorDetail property in the Instructor class
//	 Use information from the instructor class @JoinColumn
//	 To Help find associated instructor
	
	
	// Except CascadeType.REMOVE to delete instruction_detail object only not instruction object
	
	 @OneToOne(mappedBy="instructorDetail", cascade=CascadeType.ALL)
	//@OneToOne(mappedBy="instructorDetail", cascade= { CascadeType.DETACH, CascadeType.MERGE,
		//	CascadeType.PERSIST, CascadeType.REFRESH } )
	private Instructor instructor;

	public InstructorDetail() {
	}

	public InstructorDetail(String youtubeUrl, String hobby) {
		this.youtubeUrl = youtubeUrl;
		this.hobby = hobby;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getYoutubeUrl() {
		return youtubeUrl;
	}

	public void setYoutubeUrl(String youtubeUrl) {
		this.youtubeUrl = youtubeUrl;
	}

	public String getHobby() {
		return hobby;
	}

	public void setHobby(String hobby) {
		this.hobby = hobby;
	}

	public Instructor getInstructor() {
		return instructor;
	}

	public void setInstructor(Instructor instructor) {
		this.instructor = instructor;
	}

	@Override
	public String toString() {
		return "InstructorDetail [id=" + id + ", youtubeUrl=" + youtubeUrl + ", hobby=" + hobby + "]";
	}

	
}
