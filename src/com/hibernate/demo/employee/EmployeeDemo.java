package com.hibernate.demo.employee;

import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

import com.hibernate.demo.entity.Employee;

public class EmployeeDemo {

	public static void main(String[] args) {

		SessionFactory factory = new Configuration().configure("hibernate.cfg.xml")
									.addAnnotatedClass(Employee.class)
									.buildSessionFactory();
		
		Session session = factory.getCurrentSession();
		
		try {
			Employee employee = new Employee("A", "B", "AB");
			
			session.beginTransaction();
			
			session.save(employee);
			
			session.getTransaction().commit();
			
			session = factory.getCurrentSession();
			session.beginTransaction();
			
			Employee gEmployee = session.get(Employee.class, employee.getId());
			
			System.out.println(gEmployee.toString());
			
			session.getTransaction().commit();
			
			session = factory.getCurrentSession();
			
			session.beginTransaction();
			
			List<Employee> employees = session.createQuery("from Employee e where e.company='AB'").getResultList();
			
			employees.forEach(e -> System.out.println(e.toString()));
			
			session.getTransaction().commit();
			
			
			session = factory.getCurrentSession();
			session.beginTransaction();
			
			session.createQuery("delete from Employee e where e.id = 1").executeUpdate();
			
			session.getTransaction().commit();
		} finally {
			factory.close();
		}
	}

}
