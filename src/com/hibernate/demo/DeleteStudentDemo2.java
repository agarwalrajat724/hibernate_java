/**
 * 
 */
package com.hibernate.demo;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

import com.hibernate.demo.entity.Student;

/**
 * @author raagar
 *
 */
public class DeleteStudentDemo2 {

	/**
	 * @param args
	 */
	public static void main(String[] args) {

		SessionFactory factory =  new Configuration().configure("hibernate.cfg.xml")
										.addAnnotatedClass(Student.class)
										.buildSessionFactory();
		
		Session session = factory.getCurrentSession();
		
		try {
			
			
			
			
			session.beginTransaction();
			
			
			Student student = session.get(Student.class, 1);
			
			session.delete(student);
			
			System.out.println("Saving Student : " + student.toString());
			
			session.getTransaction().commit();
			
			session = factory.getCurrentSession();
			
			session.beginTransaction();
			
			session.createQuery("delete from Student where id = 2").executeUpdate();
			
			session.getTransaction().commit();
			
			System.out.println("Done");
			
		} finally {
			factory.close();
		}
	}

}
