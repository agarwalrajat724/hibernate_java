package com.hibernate.demo.OneToMany;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

import com.hibernate.demo.OneToOne.Instructor;
import com.hibernate.demo.OneToOne.InstructorDetail;

public class DeleteCourse {

	public static void main(String[] args) {

		SessionFactory factory = new Configuration().configure("hibernate.cfg.xml").addAnnotatedClass(Course.class)
				.addAnnotatedClass(Instructor.class).addAnnotatedClass(InstructorDetail.class).buildSessionFactory();

		Session session = factory.getCurrentSession();

		try {
			session.beginTransaction();
			Course course = session.get(Course.class, 10);
			
			session.delete(course);
			
			System.out.println(course);
			System.out.println(course.getInstructor());
			
			session.getTransaction().commit();

		} catch (Exception e) {
			// TODO: handle exception
		} finally {
			session.close();
			factory.close();
		}

	}

}
