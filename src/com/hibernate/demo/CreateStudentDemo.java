/**
 * 
 */
package com.hibernate.demo;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

import com.hibernate.demo.entity.Student;

/**
 * @author raagar
 *
 */
public class CreateStudentDemo {

	/**
	 * @param args
	 */
	public static void main(String[] args) {

		SessionFactory factory =  new Configuration().configure("hibernate.cfg.xml")
										.addAnnotatedClass(Student.class)
										.buildSessionFactory();
		
		Session session = factory.getCurrentSession();
		
		try {
			
			
			Student student = new Student("Hitti", "Babu", "hitti_babu@email.com");
			
			session.beginTransaction();
			
			session.save(student);
			
			System.out.println("Saving Student : " + student.toString());
			
			session.getTransaction().commit();
			
			System.out.println("Done");
			
		} finally {
			factory.close();
		}
	}

}
