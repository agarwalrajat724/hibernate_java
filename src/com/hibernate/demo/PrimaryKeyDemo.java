/**
 * 
 */
package com.hibernate.demo;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

import com.hibernate.demo.entity.Student;

/**
 * @author raagar
 *
 */
public class PrimaryKeyDemo {

	/**
	 * @param args
	 */
	public static void main(String[] args) {

		SessionFactory factory = new Configuration().configure("hibernate.cfg.xml").addAnnotatedClass(Student.class)
				.buildSessionFactory();

		Session session = factory.getCurrentSession();

		try {

			Student student1 = new Student("Rajat", "Agarwal", "agarwalrajat@email.com");
			Student student2 = new Student("Kalpesh", "Garg", "kalpeshgarg@email.com");
			Student student3 = new Student("Piyush", "Niranjan", "piyush@email.com");

			session.beginTransaction();

			session.save(student1);
			session.save(student2);
			session.save(student3);

			System.out.println("Saving Student : " + student1.toString());
			System.out.println("Saving Student : " + student2.toString());
			System.out.println("Saving Student : " + student3.toString());

			session.getTransaction().commit();

			System.out.println("Done");

		} finally {
			factory.close();
		}
	}

}
