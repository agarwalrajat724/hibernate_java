package com.hibernate.demo.OneToMany;

import java.util.ArrayList;
import java.util.Arrays;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

import com.hibernate.demo.OneToOne.Instructor;
import com.hibernate.demo.OneToOne.InstructorDetail;

public class OneToManyDemo {

	public static void main(String[] args) {

		SessionFactory factory = new Configuration().configure("hibernate.cfg.xml")
									.addAnnotatedClass(Course.class)
									.addAnnotatedClass(Instructor.class)
									.addAnnotatedClass(InstructorDetail.class)
									.buildSessionFactory();
		
		Session session = factory.getCurrentSession();
		
		try {
			session.beginTransaction();
			Instructor instructor = new Instructor("Rajakkajat", "Agarwjhkjaal", "agskahkja@gmail.com");
			
			InstructorDetail instructorDetail = new InstructorDetail("sdjajlkashd.com", "telhalkjnnis");
			
			Course course = new Course("tedjdhjhst");
			
 			instructor.add(course);
			instructor.setCourses(new ArrayList<Course>(Arrays.asList(course)));
			instructor.setInstructorDetail(instructorDetail);
			System.out.println(" Instructor : " + instructor.toString());
			System.out.println("Details : " + instructor.getInstructorDetail());
			
			System.out.println(" Courses : " + instructor.getCourses());
			session.save(instructor);
			session.save(course);
			
			session.getTransaction().commit();
		} catch (Exception e) {
			// TODO: handle exception
		} finally {
			session.close();
			factory.close();
		}
	}

}
