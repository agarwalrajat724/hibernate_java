package com.hibernate.demo.OneToOne;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

public class DeleteDemo {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		SessionFactory sessionFactory = new Configuration().configure("hibernate.cfg.xml")
				.addAnnotatedClass(Instructor.class).addAnnotatedClass(InstructorDetail.class).buildSessionFactory();

		Session session = sessionFactory.getCurrentSession();

		try {
			
			session.beginTransaction();
			
//			Instructor instructor = session.get(Instructor.class, 1);
//			
//			if(null != instructor) {
//			session.delete(instructor);
//			}
			
			InstructorDetail instructorDetail = session.get(InstructorDetail.class, 3);
			
			
			if(null != instructorDetail) {
				

				System.out.println(instructorDetail);
				System.out.println(instructorDetail.getInstructor());
				
				
				// remove the associated object reference break bi-directional link for instruction_detail object only
				// delete
				instructorDetail.getInstructor().setInstructorDetail(null);
				
				session.delete(instructorDetail);
			}
			
			session.getTransaction().commit();
			
			System.out.println("Done!!");

		} catch (Exception e) {
				System.out.println(e.getMessage());
		} finally {
			session.close();
			sessionFactory.close();
		}
	}

}
