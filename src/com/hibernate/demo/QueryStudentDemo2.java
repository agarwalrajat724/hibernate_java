/**
 * 
 */
package com.hibernate.demo;

import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

import com.hibernate.demo.entity.Student;

/**
 * @author raagar
 *
 */
public class QueryStudentDemo2 {

	/**
	 * @param args
	 */
	public static void main(String[] args) {

		SessionFactory factory =  new Configuration().configure("hibernate.cfg.xml")
										.addAnnotatedClass(Student.class)
										.buildSessionFactory();
		
		Session session = factory.getCurrentSession();
		
		try {
			session.beginTransaction();
			
			List<Student> students1 = session.createQuery("from Student s where s.lastName = 'Garg' ").getResultList();
			students1.forEach(s -> {
				System.out.println(s);
				System.out.println("TTTTTTTTTTTTTTTTTTTTTTTTTTT");
				System.out.println();
			});
					
			List<Student> students2 = session.createQuery("from Student s where s.email LIKE '%email.com' ").getResultList();
			students2.forEach(s -> {
				System.out.println(s);
				System.out.println("YYYYYYYYYYYYYYYYYYYYYYYYYYYY");
			});
			
			System.out.println("Printing all the students.....");
			List<Student> students = session.createQuery("from Student").getResultList();
			students.forEach(s -> System.out.println(s));
					
			session.getTransaction().commit();
			
		} finally {
			factory.close();
		}
	}

}
