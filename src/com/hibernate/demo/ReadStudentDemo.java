/**
 * 
 */
package com.hibernate.demo;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

import com.hibernate.demo.entity.Student;

/**
 * @author raagar
 *
 */
public class ReadStudentDemo {

	/**
	 * @param args
	 */
	public static void main(String[] args) {

		SessionFactory factory = new Configuration().configure("hibernate.cfg.xml").addAnnotatedClass(Student.class)
				.buildSessionFactory();

		Session session = factory.getCurrentSession();

		try {

			Student student = new Student("Read", "Test", "read@email.com");

			session.beginTransaction();

			session.save(student);

			session.getTransaction().commit();

			
			session = factory.getCurrentSession();
			
			session.beginTransaction();

			Student myStudent = session.get(Student.class, student.getId());

			System.out.println(myStudent.toString());

			session.getTransaction().commit();

		} finally {
			factory.close();
		}
	}

}
