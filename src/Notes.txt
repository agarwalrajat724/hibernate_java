Entity Class: Java Class that is mapped to a database table.

QUESTION:
Why we are using JPA Annotation instead of Hibernate ?

For example, why we are not using this org.hibernate.annotations.Entity?

ANSWER:
JPA is a standard specification. Hibernate is an implementation of the JPA specification.

Hibernate implements all of the JPA annotations.

The Hibernate team recommends the use of JPA annotations as a best practice.

SessionFactory:- Reads the hibernate config file Creates Session objects
				 Heavy-weight object. Only create once in your app.
				 
Session: Wraps a JDBC connection. Main object used to save/retrieve objects.
		 Short lived object. Retrieved from SessionFactory.
		 

Primary Key :- Uniquely identifies each row in a Table
Must be a unique value. Cannot contain NULL values.

@GeneratedValue(strategy=GenerationType.IDENTITY)-> Let MySQL handle the generation AUTO_INCREMENT


CREATE DATABASE  IF NOT EXISTS `hb_student_tracker`;
USE `hb_student_tracker`;

--
-- Table structure for table `student`
--

DROP TABLE IF EXISTS `student`;

CREATE TABLE `student` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `first_name` varchar(45) DEFAULT NULL,
  `last_name` varchar(45) DEFAULT NULL,
  `email` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=latin1;

SELECT * FROM student;

ALTER TABLE hb_student_tracker.student AUTO_INCREMENT = 3000

TRUNCATE hb_student_tracker.student




Cascade
1) We can cascade operations
2) Apply the same operations to related entities.
3) By Default no Operations are cascaded.
Save Instructor and Instructor_Detail will be saved too.

Cascade Delete :- Depends on the use case.
For Eg:- Many To Many between Students and Courses.

Fetch Types: Eager vs Lazy Loading
When we fetch/retrieve data should we retrieve EVERYTHING ?
-> Eager will retrieve EVERYTHING
-> Lazy will retrieve on request

Instructor - Course
		   - Course
		   -Course
		   
Eager will load all the Courses at once


To Use Bi-Directional Schema, we can keep the existing database schema, no changes required to database

More on mappedBy
 - mappedBy tells Hibernate
 Look at the instructorDetail property in the Instructor class
 Use information from the instructor class @JoinColumn
 To Help find associated instructor
 
 
 public class Instructor {
 	@OneToMany(mappedBy="instructor")
 		private List<Course> courses;
 	}
 	
 public class Course {
 	
 	@ManyToOne
 	@JoinColumn(name = "instructor_id")
 	private Instructor instructor;
 	
 }
 
 - mappedBy tells Hibernate
 Look at the instructor property in the Course class
 Use information from the Course class @JoinColumn
 To Help find associated courses for instructor
 
 




