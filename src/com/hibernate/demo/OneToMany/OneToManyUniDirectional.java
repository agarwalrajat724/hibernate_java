package com.hibernate.demo.OneToMany;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

import com.hibernate.demo.OneToOne.Instructor;
import com.hibernate.demo.OneToOne.InstructorDetail;

public class OneToManyUniDirectional {

	public static void main(String[] args) {

		SessionFactory factory = new Configuration().configure("hibernate.cfg.xml").addAnnotatedClass(Course.class)
				.addAnnotatedClass(Instructor.class).addAnnotatedClass(InstructorDetail.class)
				.addAnnotatedClass(Review.class).buildSessionFactory();

		Session session = factory.getCurrentSession();

		try {
			session.beginTransaction();
			
			Course tempCourse = new Course("Paceman");
			
			tempCourse.addReview(new Review("Great"));
			tempCourse.addReview(new Review("jjahja"));
			tempCourse.addReview(new Review("jjaasasahja"));
			
			session.save(tempCourse);
			
			
			System.out.println("Course : " + tempCourse.toString());
			session.getTransaction().commit();
			
			System.out.println("DONE !!!");
			
		} catch (Exception e) {
			// TODO: handle exception
		} finally {
			session.close();
			factory.close();
		}
	}

}
